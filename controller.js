const johnny = require("johnny-five");
const net = require('net');
const { board, joystick, lcd } = require('./lib/robotter');
const { initLcd, forwardData, printData } = require('./lib/helpers');
const {  socketClient, echoServer } = require('./lib/sockets');
const { directions } = require('./lib/directions');

var PORT = 8000;
var HOST = '192.168.1.138';
//var HOST = 'localhost'
//let mockServer = echoServer(HOST, PORT);

const cfgBoard = {
    port: '/dev/ttyUSB0'  
};

const cfgJoystick1 = {
    pins: ["A0", "A1"],  // [ x, y ]
    invert: false
};

const cfgJoystick2 = {
    pins: ["A2", "A3"], // [ x, y ]
    invert: false
};

const cfgLcd = {
    controller: "JHD1313M1"
};

const robotter = async () => {
    const [client, robo] = await Promise.all([
        socketClient(HOST, PORT), 
        board(johnny.Board, cfgBoard)
    ]);
    const display = lcd(johnny.LCD, cfgLcd, initLcd);
    joystick(johnny.Joystick, cfgJoystick1, directions.drive, [ printData(display), forwardData(client) ]);
    joystick(johnny.Joystick, cfgJoystick2, directions.cam, [ printData(display), forwardData(client) ]);
};

robotter();