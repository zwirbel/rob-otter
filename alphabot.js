const johnny = require("johnny-five");
const raspi = require("raspi-io");
const { board, joystick, lcd } = require('./lib/robotter');
const { socketClient, echoServer } = require('./lib/sockets');

const HOST = '127.0.0.1';
const PORT = '9000';

const cfgBoard = {
    io: new raspi()
};

let server = echoServer(HOST, PORT);

// def __init__(self,ain1=12,ain2=13,ena=6,bin1=20,bin2=21,enb=26):
//def forward(self):
//    self.PWMA.ChangeDutyCycle(self.PA)
//    self.PWMB.ChangeDutyCycle(self.PB)
//    GPIO.output(self.AIN1,GPIO.LOW)
//    GPIO.output(self.AIN2,GPIO.HIGH)
//    GPIO.output(self.BIN1,GPIO.LOW)
//    GPIO.output(self.BIN2,GPIO.HIGH)

const demo = motor => {
    motor.on("start", function() {
        console.log("start", Date.now());
    
        // Demonstrate motor stop in 2 seconds
        board.wait(2000, function() {
          motor.stop();
        });
      });
    
      // "stop" events fire when the motor is started.
      motor.on("stop", function() {
        console.log("stop", Date.now());
      });
    
      // Motor API
    
      // start()
      // Start the motor. `isOn` property set to |true|
      motor.start();
};

/*
 | Control Type/Role | Johnny-Five Motor Pin Name | Breakout Printed Pin |
 | ----------------- | -------------------------- | -------------------- | 
 | PWM               | pwm                        | PWMA or PWMB         | 
 | Counter Direction | cdir                       | AIN2 or BIN2 | 
 | Direction | dir | AIN1 or BIN1 |

*/

const robotter = async () => {
    const [robo] = await Promise.all([
        board(johnny.Board, cfgBoard)
    ]);
    const motor = new johnny.Motor({
        pins: [6, 12, 13],
        controller: "PCA9685",
        address: 0x40
    });
    demo(motor);
};