let { check, positive, negative, checkable, checkables } = require('../lib/common');

test('positive yields true', () => {
    expect(positive(1)).toBe(true);
});

test('positive with offset yields true', () => {
    expect(positive(11, 10)).toBe(true);
});

test('positive yields false', () => {
    expect(positive(-1)).toBe(false);
});

test('positive with offset yields false', () => {
    expect(positive(1, 10)).toBe(false);
});

test('negative yields true', () => {
    expect(negative(-1)).toBe(true);
});

test('negative yields false', () => {
    expect(negative(1)).toBe(false);
});

test('negative with offset yields true', () => {
    expect(negative(-11, 10)).toBe(true);
});

test('negative with offset yields false', () => {
    expect(negative(-9, 10)).toBe(false);
});

test('get item from checkables yields valid item', () => {
    let fnc   = val => val;
    let item1 = checkable('item1', ({ x }) => check(fnc)(true));
    let item2 = checkable('item2', ({ x }) => check(fnc)(false));
    let refs  = checkables([item1, item2]);
    expect(
        refs.get(true)
    ).toBe(item1);
});