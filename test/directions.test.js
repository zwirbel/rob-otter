const { directions } = require('../lib/directions');

test('get drive direction with positive x coord yields TurnRight', () => {
    expect(directions.drive.get({x: 11, y: 0})).toHaveProperty('data', 'TurnRight');
});

test('get drive direction with positive y coord yields Forward', () => {
    expect(directions.drive.get({x: 0, y: 11})).toHaveProperty('data', 'Forward');
});

test('get drive direction with positive x coord yields Right', () => {
    expect(directions.cam.get({x: 11, y: 0})).toHaveProperty('data', 'Right');
});

test('get cam direction with positive y coord yields Up', () => {
    expect(directions.cam.get({x: 0, y: 11})).toHaveProperty('data', 'Up');
});