const Cylon = require('cylon');
const { initLcd, forwardData, printData } = require('./lib/helpers');
const {  socketClient, echoServer } = require('./lib/sockets');


var HOST = '192.168.1.138';
//var HOST = 'localhost'
var PORT = 8000;

const THRESHOLD = 100;

const SPHERE_DRIVE = {
  'TurnRight':    (x, z, y) => x > 0 + THRESHOLD && z > THRESHOLD,
  'TurnLeft':     (x, z, y) => x < 0 - THRESHOLD && z > THRESHOLD,
  'Forward':  (x, z, y) => y < 0 - THRESHOLD && z > THRESHOLD,
  'ForwardRight':  (x, z, y) => y < 0 - THRESHOLD && x > 0 + THRESHOLD && z > THRESHOLD,
  'ForwardLeft':  (x, z, y) => y < 0 - THRESHOLD && x < 0 - THRESHOLD && z > THRESHOLD,
  'Backward': (x, z, y) => y > 0 + THRESHOLD  && z > THRESHOLD,
  'Stop':     (x, z, y) => z < THRESHOLD
};
const SPHERE_CAM = {
  'Right':    (x, z, y) => x > 0 + THRESHOLD && z > THRESHOLD,
  'Left':     (x, z, y) => x < 0 - THRESHOLD && z > THRESHOLD,
  'Up':  (x, z, y) => y < 0 - THRESHOLD && z > THRESHOLD,
  'Down': (x, z, y) => y > 0 + THRESHOLD  && z > THRESHOLD,
  'Stop':     (x, z, y) => z < THRESHOLD
};

const forwardSpherePosition = (client, payload, directions, lastDirection) => {
  for(let direction in directions){
    let [x, z, y] = payload.palmPosition;
    let canMove = directions[direction](x,z,y);
    if(canMove && lastDirection !== direction){
      lastDirection = direction;
      forwardData(client)({data: direction});
    }
  }
};

const HANDLERS = {
  right: (client, payload, lastDirection) => {
    forwardSpherePosition(client, payload, SPHERE_DRIVE, lastDirection);
  },
  left: (client, payload, lastDirection) => {
    forwardSpherePosition(client, payload, SPHERE_CAM, lastDirection);
  }
};

const robotter = async () => {
  const [client] = await Promise.all([
    socketClient(HOST, PORT)
  ]);
  Cylon.robot({
    connections: {
      leapmotion: { adaptor: 'leapmotion' }
    },
  
    devices: {
      leapmotion: { driver: 'leapmotion' }
    },
  
    work: function(my) {
      let lastDirection;
      let lastDirectionCam;
      my.leapmotion.on('hand', function(payload) {
        HANDLERS[payload.type](client, payload, payload.type === 'right' ? lastDirection : lastDirectionCam);
        //console.log(payload.toString());
        //console.log(payload.palmPosition);
      });
    }
  }).start();
};

robotter();