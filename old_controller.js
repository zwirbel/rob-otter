var net = require('net');
const johnny = require("johnny-five");

var HOST = '192.168.1.138';
var PORT = 8000;

const coords = (x, y) => ({
    y: x * -1024,
    x: y * -1024
});

const right = ({x ,y}) =>  x > 10;
const left = ({x ,y}) =>  x < -10;
const up = ({x ,y}) =>  y > 10;
const down = ({x ,y}) =>  y < -10;
const stop = ({x ,y}) => !right(x, y) && !left(x, y) && !up(x, y) && !down(x, y);

const directions = {
    'Forward' : up,
    'Backward': down,
    'TurnLeft': left,
    'TurnRight': right,
    'Stop': stop
};

const cameraDirections = {
    'Up' : up,
    'Down': down,
    'Left': left,
    'Right': right,
    'Stop': stop
};

const chars = {
    'Forward': ':pointerup:',
    'Backward': ':pointerdown:',
    'TurnLeft': ':pointerleft:',
    'TurnRight': ':pointerright:',
    'Up': ':pointerup:',
    'Down': ':pointerdown:',
    'Left': ':pointerleft:',
    'Right': ':pointerright:',
    'Stop': ':sfbox:'
};

const move = (coords, directions) => {
    for(let dir in directions) {
        if(directions[dir](coords)){
            return dir;
        }
    }
};

const print = (display, x, y, msg) => {
    //display.cursor(y, 0).print('                ');
    display.cursor(y, x).print(msg);
};

const socketClient = (host, port) => new Promise((resolve, reject) => {
    var socket = new net.Socket();
    socket.connect(port, host, function() {
        console.log('CONNECTED TO: ' + host + ':' + port);
        resolve(socket);
    });
});
const jBoard = port => new Promise((resolve, reject) => {
    let board = new johnny.Board({
        port: port
    });
    board.on('ready', () => {
        resolve(board);
    });
});

const robotter = async () => {
    let client = await socketClient(HOST, PORT);
    let board = await jBoard('/dev/ttyUSB0');
    let j1Last;
    let j2Last;

    var lcd = new johnny.LCD({
        controller: "JHD1313M1"
    });
    lcd.bgColor("blue");
    lcd.useChar("heart");
    lcd.useChar("pointerup");
    lcd.useChar("pointerdown");
    lcd.useChar("pointerleft");
    lcd.useChar("pointerright");
    lcd.useChar("sfbox");
    
    print(lcd, 0, 0, `Connected to`);
    print(lcd, 0, 1, HOST);
    lcd.clear();

    var joystick1 = new johnny.Joystick({
        //   [ x, y ]
        pins: ["A0", "A1"],
        invert: false
    }).on("change", function () {
        let dir = move(coords(this.x, this.y), directions);
        if(j1Last !== dir) {
            client.write(dir);
            j1Last = dir;
        }
        print(lcd, 0, 0, `M: ${chars[dir]}`);
    });
    var joystick2 = new johnny.Joystick({
        //   [ x, y ]
        pins: ["A2", "A3"],
        invert: false
    }).on("change", function () {
        let dir = move(coords(this.x, this.y), cameraDirections);
        if(j2Last !== dir) {
            client.write(dir);
            j2Last = dir;
        }
        print(lcd, 0, 1, `C: ${chars[dir]}`);
    });

};

robotter();