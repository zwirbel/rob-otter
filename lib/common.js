const DEFAULT_OFFSET = 0;

module.exports.check        = fnc => val => fnc(val);
module.exports.positive     = (val, offset = DEFAULT_OFFSET) => val > 0 + offset;
module.exports.negative     = (val, offset = DEFAULT_OFFSET) => val < 0 - offset;
module.exports.checkable    = (dto, chk) => ({ data: dto, check: chk });
module.exports.checkables   = (items) => {
    return {
        items: items,
        get: (val) => items.find(el => el.check(val))
    };
};
