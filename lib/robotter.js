const board = (Board, config) => new Promise((resolve, reject) => {
    return new Board(config)
        .on('ready', () => {
            resolve(board);
        })
        .on('error', (err) => {
            reject(err);
        });
});

const convertCoords = ({x, y}) => ({
    y: x * -1024,
    x: y * -1024
});

const handleCheckableData = (callables, checkables, converter = d => d) => data => {
    console.log(data);
    return callables.forEach(
        fnc => fnc(
            checkables.get(
                converter(data)
            )
        ));
}

const joystick = (Joystick, config, directions, handlers) => {
    return new Joystick(config).on("change", handleCheckableData(handlers, directions, convertCoords));
};
const lcd = (LCD, config, init) => {
    let display = new LCD(config);
    init(display);
    return display;
};

module.exports.board = board;
module.exports.lcd = lcd;
module.exports.joystick = joystick;