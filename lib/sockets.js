const net = require('net');

module.exports.socketClient = (host, port) => new Promise((resolve, reject) => {
    var socket = new net.Socket();
    socket.connect(port, host, function() {
        console.log('CONNECTED TO: ' + host + ':' + port);
        socket.on('data', (data) => {
            console.log('Received: ' + data);
        });
        socket.on('error', (err) => {
            reject(err);
        });
        resolve(socket);
    });

});

module.exports.echoServer = (host, port) => {
    return net.createServer((socket) => {
        socket.write('Echo server\r\n');
        socket.pipe(socket);
    }).listen(port, host);
};