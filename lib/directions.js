const { check, positive, negative, checkable, checkables } = require('./common');

const checkPositiveX = ({ x }) => check(positive)(x);
const checkNegativeX = ({ x }) => check(negative)(x);
const checkPositiveY = ({ y }) => check(positive)(y);
const checkNegativeY = ({ y }) => check(negative)(y);
const checkPositiveXY = (pos) => checkPositiveX(pos) && checkPositiveY(pos);
const checkPositiveYNegativeX = (pos) => checkNegativeX(pos) && checkPositiveY(pos);
const checkPositiveXNegativeY = (pos) => checkPositiveX(pos) && checkNegativeY(pos);
const checkNegativeXY = (pos) => checkNegativeX(pos) && checkNegativeY(pos);
const checkCenter = ({ x, y }) => !checkPositiveX(x, y) && !checkNegativeX(x, y) && !checkPositiveY(x, y) && !checkNegativeY(x, y);
const mapLabelsToCheckables = (labels, checks) => checkables(
	labels.map((label, idx) => checkable(label, checks[idx]))
);

// TODO also implement checks with Z axis
const positionChecks = [
	checkPositiveXY,
	checkPositiveYNegativeX,
	checkPositiveXNegativeY,
	checkNegativeXY,
	checkPositiveY,
	checkNegativeY,
	checkPositiveX,
	checkNegativeX,
	checkCenter
];

const camPositionChecks = [
	checkPositiveY,
	checkNegativeY,
	checkPositiveX,
	checkNegativeX,
	checkCenter	
];

const labels = {
	drive: [
		'ForwardRight',
		'ForwardLeft',
		'BackwardRight',
		'BackwardLeft',
		'Forward',
		'Backward',
		'TurnRight',
		'TurnLeft',
		'Stop'
	],
	cam: [
		'Up',
		'Down',
		'Right',
		'Left',
		'Stop'
	]
};

const directions = {
	drive: mapLabelsToCheckables(labels.drive, positionChecks),
	cam: mapLabelsToCheckables(labels.cam, camPositionChecks)
};

module.exports.directions = directions;
module.exports.labels = labels;