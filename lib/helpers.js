module.exports.forwardData = (client) => msg => {
    if(msg){
        client.write(msg.data);
    }
};
module.exports.printData = (display) => msg => {
    display.cursor(0, 0).print('                ');
    if(msg){
        display.cursor(0, 0).print(msg.data);
    }
};
module.exports.initLcd = display => {
    display.bgColor("blue");
};